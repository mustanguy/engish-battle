<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Game;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted(['ROLE_USER'])) {
            return $this->redirectToRoute('battle_page');
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    /**
     * @Route("/search-city", name="search-city")
     */
    public function searchCityAction(Request $request)
    {
        $q = $request->query->get('q');
        $citys = $this->getDoctrine()->getRepository('AppBundle:City')->searchCity($q);

        $cityTab = [];
        foreach ($citys as $key => $city) {
            $cityTab[$city->getId()] = $city->getNom();
        }
        return new JsonResponse($cityTab);
    }

    /**
     * @Route("/battle", name="battle_page")
     */
    public function battlePageAction(Request $request)
    {
        $user = $this->getUser();
        $leaders = $this->getDoctrine()->getRepository('AppBundle:Game')->findByMaxScore();
        return $this->render('default/battle.html.twig', ['user' => $user, 'leaders' => $leaders]);
    }

    public function confirmedAction()
    {
        return $this->redirectToRoute('battle_page');
    }

    /**
     * @Route("/create-game", name="create_game")
     */
    public function createGameAction(Request $request)
    {
        $game = new Game();
        $game->setUser($this->getUser());
        $em = $this->getDoctrine()->getManager();
        $em->persist($game);
        $em->flush();

        return new JsonResponse($game->getId());
    }

    /**
     * @Route("/end-game", name="end_game")
     */
    public function endGameAction(Request $request)
    {

        $gameId = $request->request->get('gameId');
        $score = $request->request->get('score');
        $em = $this->getDoctrine()->getManager();
        $game = $em->getRepository('AppBundle:Game')->find($gameId);
        $game->setScore($score);
        $em->flush();

        return new JsonResponse("game ended");
    }

    /**
     * @Route("/get-word", name="get_word")
     */
    public function getWordAction(Request $request)
    {
        $wordsTab = $request->request->get('wordsTab');
        if ($wordsTab === null) {
            $id = rand(1, 161);
        } else {
            $id = rand(1, 161);
            if (count($wordsTab) !== 161) {
                while (in_array($id, $wordsTab)) {
                    $id = rand(1, 161);
                }
            }
        }

        $verbe = $this->getDoctrine()->getRepository('AppBundle:Verbe')->find($id);

        return new JsonResponse(['id' => $verbe->getId(), 'base' => $verbe->getBaseVerbale()]);
    }

    /**
     * @Route("/verify-word", name="verify_word")
     */
    public function verifyWordAction(Request $request)
    {
        $participe = $request->request->get('participe');
        $preterit = $request->request->get('preterit');
        $verbeId = $request->request->get('verbeId');

        $verbe = $this->getDoctrine()->getRepository('AppBundle:Verbe')->find($verbeId);

        $response = [];
        if ($verbe->getPreterit() === $preterit) {
            $response['preterit'] = 'success';
        } else {
            $response['preterit'] = 'error';
        }

        if ($verbe->getParticipePasse() === $participe) {
            $response['participe'] = 'success';
        } else {
            $response['participe'] = 'error';
        }


        return new JsonResponse($response);
    }
}
