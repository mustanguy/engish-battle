<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * City
 *
 * @ORM\Table(name="verbe")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VerbeRepository")
 */
class Verbe
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="base_verbale", type="string", length=100)
     */
    private $baseVerbale;

    /**
     * @var string
     *
     * @ORM\Column(name="preterit", type="string", length=100)
     */
    private $preterit;

    /**
     * @var string
     *
     * @ORM\Column(name="participe_passe", type="string", length=100)
     */
    private $participePasse;

    /**
     * @var string
     *
     * @ORM\Column(name="traduction", type="string", length=100)
     */
    private $traduction;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getBaseVerbale()
    {
        return $this->baseVerbale;
    }

    /**
     * @param string $baseVerbale
     */
    public function setBaseVerbale($baseVerbale)
    {
        $this->baseVerbale = $baseVerbale;
    }

    /**
     * @return string
     */
    public function getPreterit()
    {
        return $this->preterit;
    }

    /**
     * @param string $preterit
     */
    public function setPreterit($preterit)
    {
        $this->preterit = $preterit;
    }

    /**
     * @return string
     */
    public function getParticipePasse()
    {
        return $this->participePasse;
    }

    /**
     * @param string $participePasse
     */
    public function setParticipePasse($participePasse)
    {
        $this->participePasse = $participePasse;
    }

    /**
     * @return string
     */
    public function getTraduction()
    {
        return $this->traduction;
    }

    /**
     * @param string $traduction
     */
    public function setTraduction($traduction)
    {
        $this->traduction = $traduction;
    }

}

